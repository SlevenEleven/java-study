package com.java.education.part4;

import java.util.*;

public class Task2 {
    public static void main(String[] args) {
        // Подсчитайте повторяющиеся элементы в массиве с помощью HashMap.
        int a = 3;
        a = 3 / 2;
        List<Integer> numbers = Arrays.asList(4, 4, 5, 5, 7, 3, 8, 1, 2, 6, 8);//создал начальный массив
        HashMap<Integer, Integer> resultNumbers = new HashMap<>();// создал второй массив
        for (int i = 0; i < numbers.size(); i++) {//цикл заполнения второго массива из первого
            resultNumbers.put(i, numbers.get(i));
            //System.out.println(resultNumbers.get(i));
        }

        HashMap<Integer, Integer> result1 = new HashMap<>();//создаем массив результирующий
        for (int i = 0; i < resultNumbers.size(); i++) {//цикл идет по всем элементам массива
            if (!result1.containsKey(resultNumbers.get(i))) { //проверяем что текущий элемент еще не сравнивали
                int amount = 0;
                for (int j = 0; j <= resultNumbers.size(); j++) {
                    if (resultNumbers.get(i).equals(resultNumbers.get(j))) {
                        amount++;
                        if (amount > 1) {
                            result1.put(resultNumbers.get(i), amount);
                        }
                    }
                }
            }
        }
        System.out.println(result1);
    }
}







