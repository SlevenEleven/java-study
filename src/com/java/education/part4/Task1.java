package com.java.education.part4;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Task1 {
    public static void main(String[] args) {
//        Для двух отсортированных массивов nums1 и nums2  вернуть медианное значение двух отсортированных массивов.
//
//                Пример 1:
//
//        Ввод: nums1 = [1,3], nums2 = [2]
//
//        Вывод: 2.00000
//
//        Объяснение: объединенный массив = [1,2,3], а медиана равна 2.
//
//        Пример 2:
//
//        Ввод: nums1 = [1,2], nums2 = [3,4]
//
//        Вывод: 2.50000
//
//        Объяснение: объединенный массив = [1,2,3,4], а медиана равна (2 + 3) / 2 = 2,5.
        List<Integer> list1 = Arrays.asList(1, 3);
        List<Integer> list2 = Arrays.asList(2);
        List<Integer> result = new ArrayList<>();

        int pointer1 = 0;
        int pointer2 = 0;

        while (pointer1 < list1.size() && pointer2 < list2.size()) {
            if (list1.get(pointer1) < list2.get(pointer2)) {
                result.add(list1.get(pointer1));
                pointer1++;
            } else {
                result.add(list2.get(pointer2));
                pointer2++;
            }
        }
        if (pointer1 < list1.size()) {
            while (pointer1 < list1.size()) {
                result.add(list1.get(pointer1));
                pointer1++;
            }
        }
        if (pointer2 < list2.size()) {
            while (pointer2 < list2.size()) {
                result.add(list2.get(pointer2));
                pointer2++;
            }
        }

        int median;
        if (result.size() % 2 == 0) {
            median = (result.get(result.size() / 2) + result.get(result.size() / 2 - 1)) / 2;
        } else {
            median = result.get(result.size() / 2);
        }
        System.out.println(median);
    }
}