package com.java.education.part2;

public class HelloWorld {
    public static void main(String[] args) {
        //1.Напиши программу, которая выводит на экран надпись: "Мы обязательно изучим Java".
        System.out.println("Мы обязательно изучим Java");

        //2.Напиши программу, которая в методе main объявляет такие переменные: count типа byte, age типа int и starts типа long.
        //
        //Примечание: "объявить переменную" - значит то же, что и "создать переменную".
        byte count;
        int age;
        long starts;

        //3.Поместите код ниже в метод main и закомментируй ненужные строки кода, чтобы на экран вывелась надпись:
        //
        //2 плюс 3 равно 5
        //
        int a = 3;
        //
        int b = 2;
        //
        //System.out.print("два");
        //
        System.out.print(b);
        //
        System.out.print(" плюс ");
        //
        //System.out.print(" минус ");
        //
        System.out.print(a);
        //
        //System.out.print("три");
        //
        System.out.print(" равно ");
        //
        //System.out.print(" будет ");
        //
        //System.out.print("пять");
        //
        System.out.print(a + b);
        //
        //Примечание: комментировать строки с объявлением переменных int a и int b нельзя.

        System.out.println();
        //4.Выведите на экран диапазоны значений каждого из целочисленных примитивных типов.
        System.out.println("от " + Byte.MIN_VALUE + " до " + Byte.MAX_VALUE);
        System.out.println("от " + Short.MIN_VALUE + " до " + Short.MAX_VALUE);
        System.out.println("от " + Integer.MIN_VALUE + " до " + Integer.MAX_VALUE);
        System.out.println("от " + Long.MIN_VALUE + " до " + Long.MAX_VALUE);


        //5.Реализуйте все месяца года (январь, февраль ..) в рамках Java программы и напечатайте месяц своего рождения.
        System.out.print(Months.MARCH);
    }
}
