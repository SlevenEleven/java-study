package com.java.education.lesson4.task1;

//import java.sql.SQLOutput;

//        Создать класс Person, который содержит:
//        a) поля fullName, age.
//        б) методы move() и talk(), в которых просто вывести на консоль сообщение -"Такой-то  Person говорит".
//                в) Добавьте два конструктора  - Person() и Person(fullName, age).
//        Создайте два объекта этого класса (в методе main). Один объект инициализируется конструктором Person(), другой - Person(fullName, age).
//                Для удобства рекомендуется сделать папку lesson4 внутри которой сделать папку task1 и создать отдельно класс Main и класс Person (как на практике).
class Person {
    private String fullName;
    private int age;

    Person() {
        fullName = "nobody";
    }

    Person(String fullName, int age) {
        this.fullName = fullName;
        this.age = age;
    }

    void talk(String person) {
        System.out.println("Такой-то " + person + " говорит");
    }

    void move(String person) {
        System.out.println("Такой-то " + person + " двигается");
    }
}