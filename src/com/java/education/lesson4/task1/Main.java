package com.java.education.lesson4.task1;

public class Main {
    public static void main(String[] args) {
        Person slava = new Person();
        Person vlad = new Person("Влад", 20);
        slava.move("slava");
        vlad.talk("vlad");
    }
}
