package com.java.education.lesson4.task2;

//Создать класс покупатель с полями: фамилия, имя, отчество, адрес, номер кредитной карточки, номер банковского счета.
//
//        Создать список из 5 покупателей (в main).
//        Вывести:
//
//        Имя покупателя с самой длинной фамилией
//        Адреса всех покупателей, у кого первая цифра номера кредитки 5.
//        Всех покупателей с отчеством "Евгеньевич"

public class Buyer {
    private String surname;
    private String name;
    private String middleName;
    private String address;
    private String creditCard;
    private int bankAccount;

    public Buyer(String surname, String name, String middleName, String address, String creditCard, int bankAccount) {
        this.surname = surname;
        this.name = name;
        this.middleName = middleName;
        this.address = address;
        this.creditCard = creditCard;
        this.bankAccount = bankAccount;
    }

    public int getSurNameLength() {
        return this.surname.length();
    }

    public String getSurname() {
        return this.surname;
    }

    public String getName() {
        return this.name;
    }

    public String getMiddleName() {
        return this.middleName;
    }

    public String getCreditCardNumber() {
        return this.creditCard;
    }

    public String getTheAddress() {
        return this.address;
    }
}