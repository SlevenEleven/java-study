package com.java.education.lesson4.task2;

public class Main1 {
    public static void main(String[] args) {
        Buyer[] buyer = new Buyer[5];
        buyer[0] = new Buyer("Иванов", "Иван", "Иванович", "Москва", "12345", 12345);
        buyer[1] = new Buyer("Степанов", "Степан", "Степанович", "Санкт-Питербург", "1234", 4321);
        buyer[2] = new Buyer("Сергеев", "Сергей", "Сергеевич", "Мытищи", "54532", 4321);
        buyer[3] = new Buyer("Александров", "Александр", "Александрович", "Королев", "1234", 4321);
        buyer[4] = new Buyer("Антонов", "Антон", "Евгеньевич", "Люберцы", "5666", 4321);
        int a = 0;
        for (int i = 0; i < 4; i++) {
            if (buyer[a].getSurNameLength() < buyer[i + 1].getSurNameLength()) {
                a = i + 1;
            }
        }
        System.out.println("Самая длинная фамилия у покупателя: " + buyer[a].getName());
        for (int i = 0; i < 5; i++) {
            if (buyer[i].getCreditCardNumber().charAt(0) == '5') {
                System.out.println("Номер кредитки начинается с 5 у покупателей с адресом: " + buyer[i].getTheAddress());
            }
        }
        for (int i = 0; i < 5; i++) {
            if (buyer[i].getMiddleName().equals("Евгеньевич")) {
                System.out.println("Покупатель: " + buyer[i].getSurname() + " " + buyer[i].getName() + " " + buyer[i].getMiddleName());
            }
        }
    }
}

