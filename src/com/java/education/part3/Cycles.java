package com.java.education.part3;

import java.util.Random;

public class Cycles {
    public static void main(String[] args) {
        // 1. Дано: переменная int count, начальное значение можно указать любое.
        //
        //Напишите программу, которая выводит на экран count в степени 10, если count является чётным числом, и count в степени 3, если count нечётное.
        int count = 5;
        if (count % 2 == 0) {
            count = (int) Math.pow(5, 10);
            System.out.println(count);
        } else {
            count = (int) Math.pow(5, 3);
            System.out.println(count);
        }

        // 2/ Дано: целочисленное n, начальное значение можно указать любое.
        //Найти количество натуральных чисел, не превосходящих n и не делящихся ни на одно из чисел 2, 3, 5.

        int n = 20;
        int countD = 0;
        for (int i = 0; i < n; i++) {
            if (i % 2 != 0 && i % 3 != 0 && i % 5 != 0) {
                countD++;

            }

        }


//     3.   Дано: строка str,  начальное значение можно указать любое.
//
//        Напишите программу, которая удаляет в строке все лишние пробелы, то есть серии подряд идущих пробелов заменяет на одиночные пробелы. Крайние пробелы в строке также должны удалиться.
//
//                Пример: str = " привет,   в этой    строке лишние   пробелы.  "
//
//        Результат: "привет, в этой строке лишние пробелы"

//      String str = " привет,   в этой    строке лишние   пробелы.  ";
//      while (str.contains("  "))
//        {
//           str = str.replace("  ", " ");
//        }
//      str = str.trim();
        String str = " привет,   в этой    строке лишние   пробелы.  ";
        StringBuilder builder = new StringBuilder();
        int firstSymbol = -1;
        int lastSymbol = -1;
        for (int i = 1; i < str.length(); i++) {
            if (str.charAt(i) != ' ' && str.charAt(i - 1) == ' ') {
                firstSymbol = i;
            }
            if (str.charAt(i) != ' ' && str.charAt(i + 1) == ' ') {
                builder.append(str.substring(firstSymbol, i + 1));
                builder.append(' ');
            }


        }
        String result = builder.toString().substring(0, builder.toString().length() - 1);
        System.out.println(result);


        //      StringBuilder builder = new StringBuilder();
//      int indexNew = 0;
//      if(str.charAt(0)==' ') {
//          builder.append(str.substring(1,str.length());
//      }
//      for (int b = 0; b < str.length()); b++){
//            if (str.charAt(b) == ' ') {
//            if (str.charAt(b+1)) !=
//
//            }
//        }
//
//
//
//
//       String str = " привет,   в этой    строке лишние   пробелы.  "
//       StringBuilder builder = new StringBuilder();
//       if(str.charAt(0)==' ' && str.charAt(str.length()-1)==' '){
//           builder.append(str.substring(1,str.length()-2))
//       }
//
//        }
//
//       for (int b =0; b < str.length();b++ ){
//          if (str.charAt(b) == ' '){
//              if (str.charAt(b+1)== ' ')
//          }
//       }


//     4. Дано: целочисленное число n, начальное значение может быть любое.
//
//Напишите программу, которая определяет: (можно сделать не все варианты, а выбрать понравившийся)

//а) количество цифр в нем;
        int f = 32;
        int num = (int) Math.log10(f) + 1;
//б) сумму его цифр;
        int g = 32;
        int sum = 0;
        while (g > 0) {
            sum = sum + g % 10;
            g = g / 10;
        }

//в) произведение его цифр;
        int h = 32;
        int multiPlication = 1;
        while (h > 0) {
            multiPlication = multiPlication * h % 10;
            h = h / 10;
        }


//г) среднее арифметическое его цифр;
//д) сумму квадратов его цифр;
//е) сумму кубов его цифр;
//ж) его первую цифру;
//з) сумму его первой и последней цифр.


//      5.  Дано: строка str, начальное значение может быть любое.
//
//        Напишите программу, считающую количество цифр 1, 2, 3 в строке.
//
//        Пример: str = "сегодня мы купили 1 яблоко, 1 грушу и 2 апельсина"
//
//        Кол-во 1: 2
//
//        Кол-во 2: 1
//
//        Кол-во

        String ctr = "сегодня мы купили 1 яблоко, 1 грушу и 2 апельсина";
        int countOne = 0;
        int countTwo = 0;
        for (int i = 0; i < ctr.length(); i++) {
            if (ctr.charAt(i) == '1') {
                countOne++;
            }
            if (ctr.charAt(i) == '2') {
                countTwo++;
            }
        }


    }
}





